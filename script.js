//ТЕОРІЯ
/*
Конструкція try...catch використовується при читанні з файлу або запису в файл для обробки можливих помилок, таких як відсутність файлу чи відсутність дозволів.
*/


//ПРАКТИКА
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const rootDiv = document.getElementById('root');

const ul = document.createElement('ul');

root.append(ul);

function checkProperties(book) {
    if (!book.author) {
        throw new Error("This book doesn't contain field author");
    }
    if (!book.name) {
        throw new Error("This book doesn't contain field name");
    }
    if (!book.price) {
        throw new Error("This book doesn't containfield price");
    }
}

books.forEach(book => {
    try {
        checkProperties(book);
        const li = document.createElement('li');
        li.textContent = book.author + book.name + book.price;
        ul.append(li);

    } catch (error) {
        console.log(error.message);
    }

});
